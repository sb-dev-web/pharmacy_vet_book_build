importScripts("https://www.gstatic.com/firebasejs/7.15.0/firebase-app.js");
importScripts(
  "https://www.gstatic.com/firebasejs/7.15.0/firebase-messaging.js"
);

firebase.initializeApp({
  apiKey: "AIzaSyDx2p47cSjHzEjdqLM7_CTcEktpoyE3HOQ",
  authDomain: "vetbook-fc985.firebaseapp.com",
  projectId: "vetbook-fc985",
  storageBucket: "vetbook-fc985.appspot.com",
  messagingSenderId: "930482729439",
  appId: "1:930482729439:web:03bdeac8541fbaa84f946a",
  measurementId: "G-JZR63FP8SB",
});

const messaging = firebase.messaging();

self.addEventListener("notificationclick", function (event) {
  event.notification.close();
  self.clients.openWindow(event.notification.click_action, "_blank");
});

messaging.setBackgroundMessageHandler(function (payload) {
  // console.log(
  //   "[firebase-messaging-sw.js] Received background message ",
  //   payload
  // );

  let data = payload.data;
  let title = data.title;
  let body = JSON.parse(data.body);

  // Customize notification here
  var notificationOptions = {
    body: body.notification_message,
    //    icon: '/firebase-logo.png'
    icon: "/ubergolf_logo_green_text.svg",
  };

  return registration.showNotification(title, notificationOptions);
});